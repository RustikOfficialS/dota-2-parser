import csv

import d2api

api = d2api.APIWrapper('71FE0085B46916106CF095417EED5B25')

match_id = input('Write game id: ')

match_details = api.get_match_details(match_id)


with open('match_details.csv', 'w') as csvfile:
    writer = csv.writer(csvfile, delimiter=";")
    players = match_details['players']
    winner = match_details['winner']
    duration = match_details['duration']
    start_time = match_details['start_time']
    first_blood_time = match_details['first_blood_time']
    positive_votes = match_details['positive_votes']
    negative_votes = match_details['negative_votes']
    game_mode = match_details['game_mode']
    writer.writerow(['Game Details'])
    writer.writerow([f"List of players: {players}"])
    writer.writerow([f"Side that won: {winner}"])
    writer.writerow([f"Duration: {duration}"])
    writer.writerow([f"Start time: {start_time}"])
    writer.writerow([f"First blood time: {first_blood_time}"])
    writer.writerow([f"Positive_Votes: {positive_votes}"])
    writer.writerow([f"Negative_Votes: {negative_votes}"])
    writer.writerow([f"Game mode: {game_mode}"])