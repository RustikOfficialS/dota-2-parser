import d2api

api = d2api.APIWrapper('71FE0085B46916106CF095417EED5B25')
vhs = api.get_match_history(skill = 3)

matches = [api.get_match_details(m['match_id']) for m in vhs['matches']] #matches with leavers

matches = [m for m in matches if not m.has_leavers()] #matches without leavers

print(len(matches))

print(matches[0])