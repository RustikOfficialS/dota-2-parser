import csv

import d2api
from d2api.src import entities

api = d2api.APIWrapper('71FE0085B46916106CF095417EED5B25')

match_history = api.get_match_history()

heroes = {}

freqs = []

for match in match_history['matches']:
    for player in match['players']:
        hero_id = player['hero']['hero_id']
        if not hero_id in heroes:
            heroes[hero_id] = 0
        heroes[hero_id] += 1

with open('hero_freq.csv', 'w') as csvfile:
    writer = csv.writer(csvfile, delimiter=";")
    for hero_id, freq in heroes.items():
        if not entities.Hero(hero_id)['hero_name'][14:] == '':
            writer.writerow([f"{entities.Hero(hero_id)['hero_name'][14:]}", f"{freq}"])
            freqs.append(freq)

    max_freq = max(freqs)

    for hero_id, freq in heroes.items():
        info = entities.Hero(hero_id)['hero_name'], freq
        if max_freq in info:
            writer.writerow([f"Most used hero: {entities.Hero(hero_id)['hero_name'][14:]}", f"His freq:{freq}"])